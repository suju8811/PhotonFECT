//
//  AppConfig.swift
//  Copyright © 2020 Hoang. All rights reserved.
//

import UIKit
import CoreBluetooth

let PREF_CONNECTION_STATUS  = "connection_status"
let PREF_PERIPHERAL_IDENTIFIER  = "peripheral_identifier"

var g_service: CBService? = nil
var g_peripheral: CBPeripheral? = nil
var g_characteristic: CBCharacteristic? = nil


class AppConfig: NSObject {
    
    class func setCurrentPeripheral(_ peripheral: CBPeripheral?) {
          g_peripheral = peripheral
          setCurrentPeripheralIdentifier(peripheral)
    }
    class func getCurrentPeripheral() -> CBPeripheral? {
        return g_peripheral
    }
    
    class func setCurrentPeripheralIdentifier(_ peripheral: CBPeripheral?) {
        if(peripheral != nil) {
            setStringValueForKey(key: PREF_PERIPHERAL_IDENTIFIER, val: peripheral!.identifier.uuidString)
        } else {
            setStringValueForKey(key: PREF_PERIPHERAL_IDENTIFIER, val: "aaa")
        }
    }
    
    class func getCurrentPeripheralIdentifier() -> String {
        return getStringValueForKey(key: PREF_PERIPHERAL_IDENTIFIER)
    }
    
    class func setCurrentService(_ service: CBService?) {
          g_service = service
    }
    
    class func getCurrentService() -> CBService? {
        return g_service
    }
    
    class func setCurrentCharacteristic(_ characteristic: CBCharacteristic?) {
          g_characteristic = characteristic
    }
    class func getCurrentCharacteristic() -> CBCharacteristic? {
        return g_characteristic
    }
   
    class func saveConnectionStatus(_ value: Bool) {
          setBoolValueForKey(key: PREF_CONNECTION_STATUS, val: value)
    }
    
    class func getConnectionStatus() -> Bool {
           return getBoolValueForKey(key: PREF_CONNECTION_STATUS, defaultValue: false)
       }
    
       
    class func setStringValueForKey(key: String, val: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(val, forKey: key)
        userDefaults.synchronize()
    }
    
    class func getStringValueForKey(key: String) -> String {
        let userDefaults = UserDefaults.standard
        let val = userDefaults.string(forKey: key)
        return val ?? ""
    }
    
    class func setBoolValueForKey(key: String, val: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(val, forKey: key)
        userDefaults.synchronize()
    }
    
    class func getBoolValueForKey(key: String, defaultValue: Bool) -> Bool {
        let userDefaults = UserDefaults.standard
        var val = defaultValue
        if (userDefaults.object(forKey: key) != nil) {
            val = userDefaults.bool(forKey: key)
        }
        
        return val
    }
    
    class func setIntValueForKey(key: String, val: Int) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(val, forKey: key)
        userDefaults.synchronize()
    }
    
    class func getIntValueForKey(key: String, defaultValue: Int) -> Int {
        let userDefaults = UserDefaults.standard
        var val = defaultValue
        if (userDefaults.object(forKey: key) != nil) {
            val = userDefaults.integer(forKey: key)
        }
        return val
    }
}
