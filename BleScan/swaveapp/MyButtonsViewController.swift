//
//  ButtonsViewController.swift
//  swaveapp
//
//  Created by Alex Hong on 1/7/21.
//  Copyright © 2021 Hoang. All rights reserved.
//

import UIKit
import CoreBluetooth

class MyButtonsViewController: UIViewController, PropertiesDelegate {

    @IBOutlet var onButton: UIButton!
    @IBOutlet var offButton: UIButton!
    @IBOutlet var fastButton: UIButton!
    @IBOutlet var slowButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    
        // Do any additional setup after loading the view.
    }
    
    func sendDataViaBle(_ command: String)
    {
        let dd = command.data(using: .utf8)

        if(AppConfig.getConnectionStatus() == true)
        {
            if(AppConfig.getCurrentCharacteristic() != nil) {
                let bleCharProperties:BLEDeviceProperties = BLEDeviceProperties()
                     bleCharProperties.delegate = self
                let peripheral = AppConfig.getCurrentPeripheral()!
                let charac =  AppConfig.getCurrentCharacteristic()!

                bleCharProperties.writeCharacteristicValue(peripheral:peripheral , data: dd!, char:charac , type: .withoutResponse)
            }
        }
    }

    @IBAction func onClockOn(_ sender: Any) {
        sendDataViaBle("on")
    }

    
    @IBAction func onClickOff(_ sender: Any) {
        sendDataViaBle("off")
    }
    
    
    @IBAction func onClickFast(_ sender: Any) {
        sendDataViaBle("fast")
    }
    
    @IBAction func onClickSlow(_ sender: Any) {
        sendDataViaBle("slow")
    }
    
    
    @IBAction func onClickUp(_ sender: Any) {
        sendDataViaBle("S")
    }
    
    
    
     @objc func postWriteCharacteristicValue(peripheral: CBPeripheral, char: CBCharacteristic)
    {
        
    }
    
    @objc func postWriteCharacteristicValueFailed(error: Error?)
    {
        
    }
}
