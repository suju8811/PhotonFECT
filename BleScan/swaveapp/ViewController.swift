//
//  ViewController.swift


import UIKit
import CoreBluetooth

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DeviceScannedDelegate, PairUnPairDeviceDelegate, DiscoveryDelegate {
    
    var scnCntrl: BLEDeviceScan?
    @IBOutlet var btnScanStop: UIButton!    
    @IBOutlet var btnButtons: UIButton!
    @IBOutlet weak var bleDeviceTV: UITableView!
    var discovery: BLEDeviceDiscoverServices?
    var selectedService: CBService?
    var discoveredCharacteristics: NSArray?
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var connectedPeripheral: CBPeripheral?
    var reconnectPeripheral: CBPeripheral?
    var pairController: BLEDeviceConnection?
    var sameDevice: Bool? = false
    var deviceArray: NSArray?
    var services: NSArray?
    var bluetoothOn: Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btnScanStop.setTitle("Scan/Stop", for: UIControlState.normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        self.bleDeviceTV.register(UINib(nibName: "DeviceTableViewCell", bundle: nil), forCellReuseIdentifier: "bleCell" )
        
        view.addSubview(activityIndicator)
        
        bleDeviceTV.isHidden = true
        self.setServiceNavigation()
        
        //Setting DeviceScannedDelegate
        scnCntrl = nil
        if scnCntrl == nil {
            scnCntrl = BLEDeviceScan()
        }
        scnCntrl?.delegate = self
        scnCntrl?.setDelegate()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Call back with found scanned devies
    func postScannedDevices(scannedDevices: NSArray) {
        self.activityIndicator.stopAnimating()
        deviceArray = NSArray(array: scannedDevices)
        if deviceArray != nil && (deviceArray?.count)! > 0 {
            bleDeviceTV.isHidden = false
            bleDeviceTV.reloadData()
            
            if reconnectPeripheral != nil {
                for i in (0..<(deviceArray?.count)!)
                {
                    let deviceData: DisplayPeripheral? = deviceArray?.object(at: i) as? DisplayPeripheral
                    if deviceData?.peripheral?.identifier.uuidString == reconnectPeripheral?.identifier.uuidString
                    {
                        self.view.isUserInteractionEnabled = false
                        self.activityIndicator.startAnimating()
                        if pairController == nil {
                            pairController = BLEDeviceConnection()
                        }
                        pairController?.delegate = self
                        pairController?.connectScannedDevice(peripheral:  reconnectPeripheral!, options: nil)
                        break
                    }
                }
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if deviceArray == nil ||  (deviceArray?.count)! <= 0 {
            return 0
        }
        return (deviceArray?.count)!
    }
    
    //did select row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pairedPeripheralStruct = deviceArray?.object(at: indexPath.row) as? DisplayPeripheral
        let selectedperipheral = (pairedPeripheralStruct?.peripheral!)! as CBPeripheral
        
        //Push to ServiceViewController for only paired devices
        if selectedperipheral.state.rawValue == 2 { // 2
            self.discoverServices(peripheral: self.getCurrentConnectedPeripheral()!)
        }
        
    }
    
    
    //Set current connected perpheral
    func setCurrentConnectedPeripherel(connectedPeripheral: CBPeripheral?) {
        self.connectedPeripheral = connectedPeripheral
    }
    //Get current connected perpheral
    func getCurrentConnectedPeripheral() -> CBPeripheral? {
        return self.connectedPeripheral
    }
    func setServiceNavigation() {
//        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 12.0)!, NSAttributedStringKey.foregroundColor: UIColor.black]
    }
    
    @IBAction func onClickMenu(_ sender: Any) {
        
    }
    
    //Scan for devices
    @IBAction func scanDevicesAction(_ sender: Any) {
        scnCntrl = nil
        if scnCntrl == nil {
            scnCntrl = BLEDeviceScan()
        }
        scnCntrl?.delegate = self
        if(!bluetoothOn)
        {
            let alert = UIAlertController(title: "Bluetooth", message: "Please turn bluetooth on or Bluetooth not yet ready, please try again in a few seconds...", preferredStyle: UIAlertControllerStyle.alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                self.view.isUserInteractionEnabled = true
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.scanDevicesAction), object: nil)
            return
        }
        
        self.view.isUserInteractionEnabled = true
        self.activityIndicator.startAnimating()

        
        scnCntrl?.scanDeviceByServiceUUID(serviceUUIDs: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
    
    func reconnecting_scan() {
        let uuid_string = AppConfig.getCurrentPeripheralIdentifier()
        if  uuid_string != ""
        {
            let uuid = UUID.init(uuidString: uuid_string)
            if uuid != nil
            {
                let data = BLEManager.getSharedBLEManager().centralManager?.retrievePeripherals(withIdentifiers: [uuid!])
                if data!.count > 0 {
                   reconnectPeripheral = data!.first
                    
//                   self.view.isUserInteractionEnabled = false
//                   self.activityIndicator.startAnimating()
//                   if pairController == nil {
//                       pairController = BLEDeviceConnection()
//                   }
//                   pairController?.delegate = self
//                   pairController?.connectScannedDevice(peripheral:  reconnectPeripheral!, options: nil)
                }
            }
        }
        
//        let alert = UIAlertController(title: "Bluetooth", message: "Bluetooth ready, scanning..", preferredStyle: UIAlertControllerStyle.alert)
//        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
//            self.view.isUserInteractionEnabled = true
//        })
//        alert.addAction(okAction)
//        self.present(alert, animated: true, completion: nil)
        self.view.isUserInteractionEnabled = true
        self.activityIndicator.startAnimating()
        scnCntrl = nil
        if scnCntrl == nil {
           scnCntrl = BLEDeviceScan()
        }
        scnCntrl?.delegate = self
        
        scnCntrl?.scanDeviceByServiceUUID(serviceUUIDs: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        bleDeviceTV.reloadData()
    }
 
        
    //Call back for the device bluetooth status
    func postBLEConnectionStatus(status: Int) {
        bluetoothOn = false
        if status == Int(4) { //OFF 4
           scnCntrl = nil
           if scnCntrl == nil {
               scnCntrl = BLEDeviceScan()
           }
           scnCntrl?.delegate = self
           
           scnCntrl?.scanDeviceByServiceUUID(serviceUUIDs: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
           bleDeviceTV.reloadData()
        } else if status == Int(5) {   //Powered ON
           bluetoothOn = true
           
           reconnecting_scan()
        }
    }
    
    //Call back if the device is paired successfully
    func devicePairedSuccessfully(peripheral: CBPeripheral) {
        //self.activityIndicator.stopAnimating()
        self.view.isUserInteractionEnabled = true
        bleDeviceTV.reloadData()
        reconnectPeripheral = nil
        self.setCurrentConnectedPeripherel(connectedPeripheral: peripheral)
        
        AppConfig.setCurrentPeripheral(peripheral)
        // set connect status true
        AppConfig.saveConnectionStatus(true)
//
        if(sameDevice == false) {
            //init
        }
        

        
        self.discoverServices(peripheral: peripheral)
    }
    
    @objc func pairingAction(_ sender: AnyObject) {
        self.view.isUserInteractionEnabled = false
        self.activityIndicator.startAnimating()
        let pairTag: Int? = sender.tag
        //UserDefaults.standard.integer(forKey: TAG)
        if pairTag != nil {
            let pairedPeripheralStruct = deviceArray?.object(at: pairTag!) as? DisplayPeripheral
            let selectedPeripheral = (pairedPeripheralStruct?.peripheral!)! as CBPeripheral
            
            if pairController == nil {
                pairController = BLEDeviceConnection()
            }
            pairController?.delegate = self
            if selectedPeripheral.state.rawValue == 0 {
                //Disconnecting Connected Device and connecting to another
                let connectedPeripheral = self.getCurrentConnectedPeripheral()
                if connectedPeripheral != nil {
                    sameDevice = false
                    pairController?.disConnectScannedDevice(peripheral: connectedPeripheral!)
                }
                pairController?.connectScannedDevice(peripheral: selectedPeripheral, options: nil)
            } else if selectedPeripheral.state.rawValue == 1 {
                //Connecting to a device
                pairController?.connectScannedDevice(peripheral: selectedPeripheral, options: nil)
            } else if selectedPeripheral.state.rawValue == 2 {
                //Disconnecting device
                sameDevice = true
                pairController?.disConnectScannedDevice(peripheral: selectedPeripheral)
            }
        }
    }
    
    //If connected device is unpiared successfully
    func deviceUnpairedSuccessfully(peripheral: CBPeripheral, error: Error?) {
        if sameDevice == true {
            self.activityIndicator.stopAnimating()
            sameDevice = false
            let alert = UIAlertController(title: "Unpaired", message: "Device Unpaired Successfully", preferredStyle: UIAlertControllerStyle.alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                self.view.isUserInteractionEnabled = true
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        bleDeviceTV.reloadData()
        
        AppConfig.setCurrentPeripheral(nil)
        AppConfig.setCurrentService(nil)
        AppConfig.setCurrentCharacteristic(nil)
        // set connect status false
        AppConfig.saveConnectionStatus(false)

    }
    
    //Discover all the services of the connected device
    func discoverServices(peripheral: CBPeripheral) {
        if discovery == nil {
            discovery = BLEDeviceDiscoverServices()
        }
        
        discovery?.delegate = self
        discovery?.discoverAllServices(peripheral: peripheral)
    }
    
    //If services are discovered successfully call back
    func postDiscoveredServices(discoveredServices: NSArray) {
        services = NSArray(array: discoveredServices)
        selectedService = (services?.object(at: 0) as? CBService?)!
        AppConfig.setCurrentService(selectedService)
        
        self.getChracteristicsOfServices(peripheral: self.getCurrentConnectedPeripheral()!, service: selectedService!)
    }
    
    //Discover characteristics of the selected service
    func getChracteristicsOfServices(peripheral: CBPeripheral, service: CBService) {
        if discovery == nil {
            discovery = BLEDeviceDiscoverServices()
        }
        discovery?.delegate = self
        discovery?.discoverAllCharacteristics(peripheral: self.getCurrentConnectedPeripheral()!, service: selectedService!)
    }
    // MARK: - Characteristics Call Back
    //Discover all characteristcs
    func postDiscoverdCharacteristices(discoveredCharacteristics: NSArray) {
        self.discoveredCharacteristics = NSArray(array: discoveredCharacteristics)
        AppConfig.setCurrentCharacteristic(self.discoveredCharacteristics?.object(at: 0) as? CBCharacteristic)
        self.activityIndicator.stopAnimating()
        

        if scnCntrl != nil {
            scnCntrl?.stopScan()
        }
        performSegue(withIdentifier: "gotoButtons", sender: nil)
    }
    
    //Discovery of characteristics failed
    func PostDicoverdCharacteristicesFailed(error: NSError?) {
    }
    //If device pairing failed
    func devicePairedFailed(peripheral: CBPeripheral, error: Error?) {
        bleDeviceTV.reloadData()
        self.view.isUserInteractionEnabled = true
        self.activityIndicator.stopAnimating()
        // set connect status false
        AppConfig.saveConnectionStatus(false)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DeviceTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "bleCell", for: indexPath) as? DeviceTableViewCell)!
        
        //TAGGING AND ADDING ACTION METHOD TO THE CUSTOM CELL BUTTON
        cell.pairBtn.tag = indexPath.row
        cell.pairBtn.addTarget(self, action: #selector(ViewController.pairingAction(_:)), for:UIControlEvents.touchUpInside)
        let deviceData: DisplayPeripheral? = deviceArray?.object(at: indexPath.row) as? DisplayPeripheral
        if deviceData?.peripheral != nil {
            //If device is paired
            if deviceData?.peripheral?.state.rawValue == 2 {
                cell.pairBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 109.0/255.0, blue: 0.0/255.0, alpha: 1)
                cell.pairBtn.setTitle("UNPAIR", for: UIControlState.normal)
            } else {
                cell.pairBtn.backgroundColor = UIColor(red: 168.0/255.0, green: 168.0/255.0, blue: 168.0/255.0, alpha: 1)
                cell.pairBtn.setTitle("PAIR", for: UIControlState.normal)
            }
            if deviceData?.peripheral?.name == nil {
                cell.deviceName.text = "UNKNOWN DEVICE" + String(indexPath.row)
            } else {
                cell.deviceName.text = deviceData?.peripheral?.name?.uppercased()
            }
        }
        
        return cell
    }
}

